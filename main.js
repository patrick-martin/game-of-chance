let userScore = 0;
let computerScore = 0;
const userScore_span = document.getElementById('user-score'); //underscore differentiates between normal variables and DOM variables
const computerScore_span = document.getElementById('computer-score'); //these are DOM variables--variables which store DOM elements
const scoreBoard_div = document.querySelector('.score-board');
const result_p = document.querySelector('.result >p');
const rock_div = document.getElementById('r');
const paper_div = document.getElementById('p');
const scissors_div = document.getElementById('s');

function game(userChoice) {
    const computerChoice = getComputerChoice();
    console.log("User Choice is  " + userChoice); //userChoice is already set; it is the argument passed in when calling the game() function
    console.log("Computer Choice is " + computerChoice) //computerChoice will always be random
    if (userChoice === 'r' && computerChoice === 's') {
        console.log('User wins!')
        userScore++;
        userScore_span.innerHTML = userScore;
        result_p.innerHTML = 'Rock beats Scissors.  You win!'
    }
    if (userChoice === 'r' && computerChoice === 'p') {
        console.log('Computer wins!')
        computerScore++;
        computerScore_span.innerHTML = computerScore;
        result_p.innerHTML = 'Paper covers Rock.  You lose.'
    }
    if (userChoice === 'r' && computerChoice === 'r') {
        console.log('The game is a draw.')
        result_p.innerHTML = 'The game is a draw.'
    }
    if (userChoice === 'p' && computerChoice === 'r') {
        console.log('User wins!')
        userScore++;
        userScore.innerHTML = computerScore;
        result_p.innerHTML = 'Paper covers Rock.  You win!'
    }
    if (userChoice === 'p' && computerChoice === 's') {
        console.log('Computer wins!')
        computerScore++;
        computerScore_span.innerHTML = computerScore;
        result_p.innerHTML = 'Scissors cuts Paper.  You lose.'
    }
    if (userChoice === 'p' && computerChoice === 'p') {
        console.log('The game is a draw.')
        result_p.innerHTML = 'The game is a draw.'
    }
    if (userChoice === 's' && computerChoice === 'r') {
        console.log('Computer wins!')
        computerScore++;
        computerScore_span.innerHTML = computerScore;
        result_p.innerHTML = 'Rock beats Scissors.  You lose.'
    }
    if (userChoice === 's' && computerChoice === 'p') {
        console.log('User wins!')
        userScore++;
        userScore_span.innerHTML = userScore;
        result_p.innerHTML = 'Scissors cuts Paper.  You win!'
    }
    if (userChoice === 's' && computerChoice === 's') {
        console.log('The game is a draw')
        result_p.innerHTML = 'The game is a draw.'
    }

}

function getComputerChoice() {
    let choices = ['r', 'p', 's']
    let randomNum = Math.floor(Math.random() * 3); //generates random number 0-3
    return choices[randomNum] //will grab random array element (r,p,s) when invoked
}

//When each 'div' is clicked, the game(userChoice) function is called--with userChoice parameter being the 'div' clicked ('r, 'p', 's')

rock_div.addEventListener('click', function() { //used addEventListener function on our variable objects
    game('r'); //EventListener method takes two parameters (click, function to be performed when action is taken)
})
paper_div.addEventListener('click', function() {
    game('p');
})
scissors_div.addEventListener('click', function() {
    game('s')
})